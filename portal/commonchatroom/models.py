
# Django related imports

from django.db import models
from datetime import datetime
from django.template.defaultfilters import slugify


#importing models

from users.models import MyUser


# Create your models here.


#defining predefined tags for questions
class Tag(models.Model):
    name= models.CharField(max_length=50)

    def __str__(self):
        return self.name


#Question Model to be asked by a user
class Question(models.Model):
    question_title = models.CharField(blank=True,max_length=150)
    question_text = models.TextField(blank=True)
    question_asker = models.ForeignKey(MyUser,related_name='question_asker')
    question_slug = models.SlugField(max_length=200,unique=True,blank=True)
    created_timestamp = models.DateTimeField(blank =True,auto_now_add = True)
    updated_timestamp = models.DateTimeField(blank =True,auto_now = True)
    tag = models.ManyToManyField(Tag, blank = True)
    
    #creating slug using  question title 
    def save(self, *args, **kwargs):
        if not self.id:
            
            # Newly created object, so set slug
            self.question_slug = slugify(self.question_title)

        super(Question,self).save(*args, **kwargs)


    def __str__(self):
        return self.question_title
    


#Answer model to be written by user
class Answer(models.Model):
    answer_text = models.CharField(blank=True,max_length=10000)
    question_id = models.ForeignKey(Question,related_name='question_id')
    answer_writer = models.ForeignKey(MyUser,related_name='answer_writer')
    created_timestamp = models.DateTimeField(blank =True,auto_now_add = True)
    updated_timestamp = models.DateTimeField(blank = True,auto_now = True)
    
    def __str__(self):
        return self.answer_text