'''-------------------------------------------------------------------'''

#URL module imports from django.
from django.conf.urls import include, url
from .views import *

'''-------------------------------------------------------------------'''

urlpatterns = [
    url('question', QuestionHandler.as_view(), name='question_handler'),
    #url('question_detail/(?P<slug>[-\w]+)/$',QuestionDetail.as_view(), name='question_detail'),
    url('answer',AnswerHandler.as_view(), name= 'answer_handler'),
    url('answer_detail/(?P<answer_id>\d+)/$',AnswerDetail.as_view(), name='answer_detail'),
]