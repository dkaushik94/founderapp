'''------------------------------------------------------------------------'''

#Django Related imports
import json
from django.shortcuts import render
from rest_framework.response import Response
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.authentication import Token
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework import views
from rest_framework.authentication import TokenAuthentication,BasicAuthentication


'''------------------------------------------------------------------------'''

# Model Import
from .models import *

'''------------------------------------------------------------------------'''


# Serializer import 
from .serializers import * 


'''------------------------------------------------------------------------'''


class QuestionHandler(views.APIView):

    '''
    This view contains 2 methods GET and POST

    GET  will be used to get a list of all related questions and its associated answer list

    POST will be used to create a question with all the details. The tags for the question will
    be available from a list of selected tags 

    '''

    #def GET method to retrieve a list of question and related answers
    def get(self, request):
        if request.user.is_authenticated():

            user = request.user
            founders = user.startup.related_startup.all()

            questionlist = []
            following = []
            question_data = []
            answerlist = []         
            
            '''
            getting all the startups followed by the user
            '''
            startups = user.following_startup.all()
            
            '''
            listing all the people that the logged in user is following
            '''
            for s in startups:
                following.extend(s.related_startup.all())

            '''listing out all the questions that have been asked by the user 
            in following list
            
            '''
            for u in following:
                questionlist.extend(Question.objects.filter(question_asker=u))
            
            serialized_question =  QuestionSerializer(questionlist, many=True)
            

            
            for q in questionlist:
                answerlist.extend(q.question_id.all())
                      
            serialized_answer = AnswerSerializer(answerlist, many=True)
            tags = Tag.objects.all()
            serialzed_tag = TagSerializer(tags, many=True)

            return Response({
                'success'   : True ,
                'question'  : serialized_question.data,
                'answer'    : serialized_answer.data,
                'tag'       : serialzed_tag.data,
                'status'    : status.HTTP_200_OK
                })

        else:
            # User is not authenticated
            return Response({
                'success'   : False ,
                'error'     : 'User not Authenticated. You are required to authenticate to acces this endpoint.',
                'status'    : status.HTTP_401_UNAUTHORISED
                })

    # POST method to create the quesion
    
    @authentication_classes((BasicAuthentication,TokenAuthentication,))
    def post(self,request):

        if request.user.is_authenticated():
            
            try:

                assert 'question_asker' in request.data
            
                created_question = QuestionSerializer(data = request.data)
                
                try:
                    if created_question.is_valid():
                        created_question.save()
                        
                        return Response({
                            'success'   : True,
                            'status'    : status.HTTP_200_OK,
                            'data'      : created_question.data,
                            })
                        
                    else:

                        # data is invalid
                        

                        return Response({
                            'success'       : False , 
                            'status'        : status.HTTP_400_BAD_REQUEST , 
                            'error'         : "Data is not valid , maybe some parameters are missing. Please re-check data sent.",
                            'data-errors'   : created_question.errors,

                            })

                except Exception as e:
                    return Response({
                        'success'   : False , 
                        'status'    : status.HTTP_500_INTERNAL_SERVER_ERROR , 
                        'error'     : "An Exception occerred. Please try Again:" ,
                        'data-errors': created_question.errors,
                        })



            except Exception:
                return Response({
                    'success'   : False ,
                    'error'     : 'Error' ,
                    'status'    : status.HTTP_500_INTERNAL_SERVER_ERROR
                    })
        else:

            # User is not authenticated
            return Response({
                'success'   : False ,
                'error'     : 'User not Authenticated. You are required to authenticate to acces this endpoint.',
                'status'    : status.HTTP_401_UNAUTHORISED,
                })



    @authentication_classes((TokenAuthentication,))
    def put(self , request):
        if request.user.is_authenticated():
            try:
                
                #Check if answer_id present in request.data
                
                assert 'question_id' in request.data

                try:    
                    question_id = request.data['question_id']
                    edited_question = Question.objects.get(id = question_id)
                    

                    serialized_data = QuestionEditSerializer(edited_question , data = request.data)
                    
                    if serialized_data.is_valid():
                        serialized_data.save()
                        
                        return Response({
                            'success'   : True , 
                            'data'      : serialized_data.data ,
                            'status'    : status.HTTP_200_OK,
                            })
                    else:
                        return Response({
                            'success'       : False , 
                            'status'        : status.HTTP_400_BAD_REQUEST , 
                            'error'         : "Data is not valid. Please re-check data sent.",
                            'data-errors'   : serialized_data.errors,
                            })

                except AssertionError as e:
                    return Response({
                        'status'    : status.HTTP_400_BAD_REQUEST ,
                        'error'     : "Please re-check your data. 'quesion_id' is missing." ,
                        'success'   : False,
                        })
            except Answer.DoesNotExist:
                return Response({
                    'status'    : status.HTTP_400_BAD_REQUEST ,
                    'error'     : "Please re-check your data. The given id doesn't return an answer instance." ,
                    'success'   : False,
                    })
        else:
            #User is not authentication and need to login.
            return Response({
                'success'   : False,
                'status'    : status.HTTP_401_UNAUTHORISED , 
                'error'     : "User not Authenticated. You are required to authenticate to acces this endpoint."
                })



'''CBV for QuestionDetail.'''
class QuestionDetail(views.APIView):

    '''
    this view  will be used to get details of a single question and its associated answer list
    The parameter will include a slug in the url which will be used to retrieve the question.
    The function will return True if a Question is found else will reurn False
    '''

    
    
    @authentication_classes((TokenAuthentication,))
    def get(self, request, slug):
        

        if request.user.is_authenticated():
            try:
                
                question = Question.objects.get(question_slug=slug)
                
                #Serialize the data
                serialized_question = QuestionDetailSerializer(question)
                
                #serializing the tags realted to the question
                question_tag = TagSerializer(question.tag.all(), many=True)
                
                #serializing answers related to the question
                answer = question.answer_set.all()
                serialized_answer = AnswerSerializer(answer, many=True)
                
                
                return Response({
                    'success'       : True,
                    'question'      : serialized_question.data,
                    'answer'        : serialized_answer.data,
                    'tag'           : question_tag.data,
                    'status_code'   : status.HTTP_200_OK,
                    })

            except Question.DoesNotExist:
                question = None
                answer = None
                tag = None

            
                return Response({
                    'success'       : False,
                    'question'      : question,
                    'answer'        : answer,
                    'tag'           : tag,      
                    'status_code'   : status.HTTP_404_NOT_FOUND,

                    })
        else :
            return Response({
                'success'   : False ,
                'error'     : 'User not Authenticated. You are required to authenticate to acces this endpoint.',
                'status'    : status.HTTP_401_UNAUTHORISED
                })



'''CBV for AnswerDetail.'''
class AnswerDetail(views.APIView):
    '''
    This view will contain a GET  method 

    GET method will be used to retrieve a particular answer through its id 

    '''
    
    @authentication_classes((TokenAuthentication,))
    def get(self, request, answer_id,format=None):
        
        if request.user.is_authenticated():

            try:
        
                answer = Answer.objects.get(id=answer_id)
                user = request.user
                serialzed_user = UserDetailSerializer(user)
                
                #Serializing the data
                serialized_answer = AnswerSerializer(answer)
                question =  QuestionDetailSerializer(answer.question_id)

                return Response({
                    'success'       : True,
                    'question'      : question.data,
                    'answer'        : serialized_answer.data,
                    'user'          : serialzed_user.data,
                    'status_code'   : status.HTTP_200_OK,   

                    })

            except Answer.DoesNotExist:
                question = None
                answer = None


            
                return Response({
                    'success'       : False,
                    'question'      : question,
                    'answer'        : answer,
                    'status_code'   : status.HTTP_404_NOT_FOUND,
                    })

        else :
            return Response({
                'success'   : False ,
                'error'     : 'User not Authenticated. You are required to authenticate to acces this endpoint.',
                'status'    : status.HTTP_401_UNAUTHORISED
                })





'''CBV for Answer Handler'''
class AnswerHandler(views.APIView):
    '''
    This view will conrtain a POST and PUT method


    POST method will be used to create an answer to a particular question. The fields 
    will be passed in body . the data that will be passed in body include answer_text, answer_writer
    (id of the user ie. integer value), and the question_id(integer value)

    PUT method will be used to update an answer. It will cotain the id of the answer to be edited 
    and the edited answer
    '''

    # POST method 

    
    @authentication_classes((TokenAuthentication,))
    def post(self , request):
        if request.user.is_authenticated():
            try:
                created_answer = AnswerSerializer(data=request.data)
                
                
                try:

                    if created_answer.is_valid():
                        created_answer.save()
                        
                        return Response({
                            'success'   : True ,
                            'data'      : created_answer.data,
                            'status'    : status.HTTP_200_OK,


                            })

                    else:
                        return Response({
                            'success'       : False , 
                            'status'        : status.HTTP_400_BAD_REQUEST , 
                            'error'         : "Data is not valid. Please re-check data sent.",
                            'data-errors'   : created_answer.errors,
                            })
                except Exception :

                    return Response({
                        'success'   : False , 
                        'status'    : status.HTTP_500_INTERNAL_SERVER_ERROR , 
                        'error'     : "An Exception occerred. Please try Again:" ,
                        })

            except Exception :

                return Response({
                    'success'   : False , 
                    'status'    : status.HTTP_500_INTERNAL_SERVER_ERROR , 
                    'error'     : "An Exception occerred. Please try Again:" ,
                    'data-errors': created_answer.errors,
                    })
        else:
            return Response({
                'success'   : False ,
                'error'     : 'User not Authenticated. You are required to authenticate to acces this endpoint.',
                'status'    : status.HTTP_401_UNAUTHORISED
                })



    #PUT method for editing
    @authentication_classes((TokenAuthentication,))
    def put(self , request):
        if request.user.is_authenticated():
            try:
                
                #Check if answer_id present in request.data
                
                assert 'answer_id' in request.data

                try:    
                    answer_id = request.data['answer_id']
                    edited_answer = Answer.objects.get(id = answer_id)
                    serialized_data = AnswerEditSerializer(edited_answer , data = request.data)
                    if serialized_data.is_valid():
                        serialized_data.save()
                    
                        return Response({
                            'success'   : True , 
                            'data'      : serialized_data.data ,
                            'status'    : status.HTTP_200_OK,
                            })
                    else:
                        return Response({
                            'success'       : False , 
                            'status'        : status.HTTP_400_BAD_REQUEST , 
                            'error'         : "Data is not valid. Please re-check data sent.",
                            'data-errors'   : serialized_data.errors,
                            })

                except AssertionError as e:
                    return Response({
                        'status'    : status.HTTP_400_BAD_REQUEST ,
                        'error'     : "Please re-check your data. 'post_id' is missing." ,
                        'success'   : False,
                        })
            except Answer.DoesNotExist:
                return Response({
                    'status'    : status.HTTP_400_BAD_REQUEST ,
                    'error'     : "Please re-check your data. The given id doesn't return an answer instance." ,
                    'success'   : False,
                    })
        else:
            #User is not authentication and need to login.
            return Response({
                'success'   : False,
                'status'    : status.HTTP_401_UNAUTHORISED , 
                'error'     : "User not Authenticated. You are required to authenticate to acces this endpoint."
                })
