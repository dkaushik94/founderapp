'''-----------------------------------------------------------------------------'''

#Framework Imports
from rest_framework import serializers
from django.contrib.auth import update_session_auth_hash

'''-----------------------------------------------------------------------------'''

#Importing Models
from .models import *
from users.models import MyUser

'''-----------------------------------------------------------------------------'''

#Importing Serialzers
from users.serializers import MyuserSerializer,QuestionAskerSerializerField, AnswerWriterSerializerField

'''-----------------------------------------------------------------------------'''


#Serializer class for Tag
class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
    


#Serializer class for  Posting Question
class QuestionSerializer(serializers.ModelSerializer):
    
    
    class Meta:
        model = Question
        


class QuestionDetailSerializer(serializers.ModelSerializer):
    question_asker = QuestionAskerSerializerField()
    
    
    class Meta:
        model = Question
        fields  = (('question_title','question_text', 'question_asker','question_slug', ))


#Serializer class for editing the quuestion
class QuestionEditSerializer(serializers.ModelSerializer):
    

    class Meta:
        model = Question
        exclude = ('question_asker',)
        

#Serializer class for Answer
class AnswerSerializer(serializers.ModelSerializer):
    #answer_writer = AnswerWriterSerializerField()
    class Meta:
        model = Answer
        depth = 1


class AnswerEditSerializer(serializers.ModelSerializer):
    answer_writer = AnswerWriterSerializerField(read_only=True)
    class Meta:
        model = Answer
        exclude = ('question_id',)



