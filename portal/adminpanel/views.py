'''--------------------------------------------------------------------------------------'''

#Django Package imports. 
from django.shortcuts import render,redirect
from django.core.mail import send_mail
from django.utils.crypto import get_random_string
import boto.ses
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse , JsonResponse
from django.views.decorators.csrf import csrf_exempt

import traceback
from portal import boto_helper
from portal.settings import S3_ACCESS_KEY, S3_BUCKET_NAME, S3_SECRET_ACCESS_KEY,\
                            user_images_file_path, startup_images_file_path, events_images_file_path
import os 

from PIL import Image
from io import StringIO
'''--------------------------------------------------------------------------------------'''

#Model Imports.
from posts.models import post
from startups.models import startUp
from events.models import * 
'''--------------------------------------------------------------------------------------'''

#Rest Framework Imports.
from rest_framework.response import Response

'''--------------------------------------------------------------------------------------'''

#Form Imports
from startups.forms import * 
from users.forms import *
from events.forms import * 

'''--------------------------------------------------------------------------------------'''


'''StartUp Ranking list View. FBV.'''
@login_required
@csrf_exempt
def StartUpRank(request):
    """
        Function to publish or unpublish ranks for top ten startups on the App.
    """
    if request.method == "GET":
        r_startups = startUp.objects.filter(trending = True)
        u_startups = startUp.objects.filter(trending = False)
        context = {
            'r_startups' : r_startups,
            'u_startups' : u_startups,
            }
        
        return render(request,'adminpanel/startup_ranking.html',context)

    elif request.method == "POST": 

        try:
            
            unpublish = startUp.objects.filter(trending = True)
            '''Reset current rank list.'''
            for u in unpublish:
                u.trending = False
                u.rank = None
                u.save()
            
            i = 1
            '''Set Current Rank List.'''
            for u in request.POST.getlist("publish_array"): 
                startup = startUp.objects.get(name_of_startup = u)
                startup.trending = True
                startup.rank = i
                startup.save()
                i= i+1

            return JsonResponse({'success' : True})
        except Exception as e :
            return JsonResponse({"success" : False})

def StartupRequest(request):

    '''
        Function for a list of startups whose request is pending 
        or whose request has been approved
    '''

    approved_startups = startUp.objects.filter(approval_status='Approved')
    pending_startups = startUp.objects.filter(approval_status='Pending')

    context = {
            'approved_startups' : approved_startups,
            'pending_startups'  : pending_startups,

    }
    return render(request,'adminpanel/startuprequests.html',context)

def StartupDetail(request, startup_name):


    startup = startUp.objects.get(name_of_startup=startup_name)
    users = startup.cofounders.all()
    context = {

            'startup'   : startup,
            'users'     : users,
    }
    return render(request,'adminpanel/startupdetails.html',context)

def StartupApprove(request, startup_name):
    '''
    Function for approving the Startup
    Invite token is generated and sent to the Founders 
    '''
    
    startup = startUp.objects.get(name_of_startup=startup_name)
    users = startup.cofounders.all()
    
    startup.approval_status = True
    # establishing connection to AWS SES
    conn = boto.ses.connect_to_region(
                'us-west-2',
                aws_access_key_id='AKIAI6WOXTRMWMPR65QQ',
                aws_secret_access_key='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')

        
    for user in users:
        
        #Generating a random 8_length token string
        invite_token = get_random_string(length=8)
        
        #setting the invite token as password   
        user.set_password(invite_token)
        user.save()

        html =  '<html><head><title>Approved</title><head><body> ' +\
                '<div><p>Hello your request for Founders has been approved.'+\
                'Below are your attached username and password <br> username: '+ \
                 user.username + '<br> password: ' + invite_token +  '<br>'+ \
                'You can now login to the app <br><br>Regards<br>Founder</body></html>'
        
        
        # send mail code 
        # email id to be verified in testing process
        conn.send_email(
                'karan@grappus.com',
                'Request Approved',
                 None,
                 [user.email],
                 format='html',
                 html_body = html,
              ) 

    return render(request,'adminpanel/mail_sent.html')

@csrf_exempt
def AddStartup(request):

    
    if request.method == 'POST':


        file_path = startup_images_file_path
        
        form = AddStartupForm(request.POST , request.FILES)
        if form.is_valid():
            name_of_startup      = form.cleaned_data['name_of_startup'] 
            linkedin_url         = form.cleaned_data['linkedin_url']
            facebook_url         = form.cleaned_data['facebook_url']
            twitter_handle       = form.cleaned_data['twitter_handle']
            inc_year             = form.cleaned_data['inc_year']
            type_of_trade        = form.cleaned_data['type_of_trade']
            description          = form.cleaned_data['description']
            team_size            = form.cleaned_data['team_size']
            location             = form.cleaned_data['location']
            company_tagline      = form.cleaned_data['company_tagline']

            background_image     = request.FILES['background_image']
            
            startup_logo         = request.FILES['startup_logo']
            

            #reading background image and uploading to s3 and saving the url to databse
            background_file_name =  str(background_image)
            background_image_file = open(os.path.join(file_path, background_file_name), 'wb')
            background_image_file.write(background_image.read())
            background_image_file.close()
            background_image_path = boto_helper.upload_image(file_path, str(background_image))
            background_image_s3 = "https://s3.amazonaws.com/" + S3_BUCKET_NAME + "/" + background_file_name
            
            #reading background image and uploading to s3 and saving the url to databse
            startup_logo_file_name =  str(startup_logo)
            startup_logo_file = open(os.path.join(file_path, startup_logo_file_name), 'wb')
            startup_logo_file.write(startup_logo.read())
            startup_logo_file.close()
            startup_logo_path = boto_helper.upload_image(file_path, str(startup_logo))
            startup_logo_s3 = "https://s3.amazonaws.com/" + S3_BUCKET_NAME + "/" + startup_logo_file_name
            
            startup = startUp.objects.create(name_of_startup=name_of_startup, linkedin_url=linkedin_url, \
                            facebook_url=facebook_url, twitter_handle=twitter_handle, inc_year = inc_year,\
                            description= description, team_size=team_size, location=location,\
                            company_tagline=company_tagline, background_startup_image= background_image_s3, \
                            startup_logo = startup_logo_s3)


            return redirect('/adminpanel/startups/add/founders')
        else:
            
            return render(request,'adminpanel/addstartup.html', {'form':form, 'errors' : form.errors})

        
    else:
        form = AddStartupForm()
        return render(request,'adminpanel/addstartup.html', {'form':form})

@csrf_exempt
def AddFounder(request):


    file_path = user_images_file_path
    if request.method == 'POST':
        form = AddUserForm(request.POST , request.FILES)
        
        if form.is_valid():
            add_another_founder = form.cleaned_data['add_another_founder']
            first_name          = form.cleaned_data['first_name']
            last_name           = form.cleaned_data['last_name']
            email               = form.cleaned_data['email']
            startup             = form.cleaned_data['startup']
            linkedin_url        = form.cleaned_data['linkedin_url']
            facebook_url        = form.cleaned_data['facebook_url']
            twitter_handle      = form.cleaned_data['twitter_handle']
            image               = request.FILES['image']
            
            file_name =  str(image)
            image_file = open(os.path.join(file_path, file_name), 'wb')
            image_file.write(image.read())
            image_file.close()
            image_path = boto_helper.upload_image(file_path, str(image))
            image_s3 = "https://s3.amazonaws.com/" + S3_BUCKET_NAME + "/" + file_name

            startup = startUp.objects.get(name_of_startup = startup)
            
            user = MyUser.objects.create(username=email,email = email, first_name = first_name,\
                        last_name= last_name , facebook_url= facebook_url, linkedin_url= linkedin_url,\
                        twitter_handle = twitter_handle, image = image_s3, startup = startup)
            

            if add_another_founder:
                return redirect('/adminpanel/startups/add/founders')
            else:
                return redirect('/admin')

        else:
            return render(request,'adminpanel/addfounders.html',{'form':form,'errors':form.errors})
    else:
        form = AddUserForm()
        return render(request,'adminpanel/addfounders.html',{'form':form})

def AddEvent(request):

    file_path = events_images_file_path

    if request.method == 'POST':
        form = AddEventForm(request.POST , request.FILES)


        name_of_event   = form.cleaned_data['name_of_event']
        hosted_by       = form.cleaned_data['hosted_by']
        description     = form.cleaned_data['description']
        location        = form.cleaned_data['location']
        start_date      = form.startdate['start_date']
        end_date        = form.cleaned_data['end_date'] 
        public_url      = form.cleaned_data['public_url']
        event_image     = request.FILES['image']
        file_name =  str(event_image)
        image_file = open(os.path.join(file_path, file_name), 'wb')
        image_file.write(event_image.read())
        image_file.close()
        image_path = boto_helper.upload_image(file_path, str(event_image))
        image_s3 = "https://s3.amazonaws.com/" + S3_BUCKET_NAME + "/" + file_name

        event = Event.objects.create(name_of_event=name_of_event, hosted_by=hosted_by, location=location,\
                            description=description, start_date=start_date, end_date=end_date,\
                             public_url=public_url, event_image = image_s3)
        
        return redirect('/admin')
    
    else:
        form = AddEventForm()
        return render(request, 'adminpanel/addevent.html', {'form':form})
