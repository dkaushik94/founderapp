"""ADMIN SPECIFIC URLS"""

'''---------------------------------------------------------------------------------------------------------------------'''

#Django Import.
from django.conf.urls import include,url

'''---------------------------------------------------------------------------------------------------------------------'''

#Views imports.
from .views import *

'''---------------------------------------------------------------------------------------------------------------------'''

urlpatterns = [

	url(r'rank_list/' , StartUpRank , name = 'Rank_List'),
	url(r'startups/requests/$', StartupRequest, name = 'startup_requests'),
	url(r'startups/(?P<startup_name>\w+)/$', StartupDetail, name = 'startup_detail'),
	url(r'startups/add/startup', AddStartup, name='add_startup'),
	url(r'startups/add/founders', AddFounder, name='add_founder'),
	url(r'startups/approve/(?P<startup_name>\w+)/$',StartupApprove, name='startup_approval'),
	url(r'add_event/', AddEvent, name = 'add_event')


]
